#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 38  115  85 }



historical_idea_groups = {
	economic_ideas
	religious_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	aristocracy_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Should use the same regnal numbers as the Ynnic Empire
	#Generic Sarda Names
	"Adrahel #0" = 50
	"Alaran #0" = 50
	"Valdaran #0" = 50
	"Auryel #0" = 50
	"Alvyr #0" = 50
	"Andralan #0" = 50
	"Arkanvyr #0" = 50
	"Ardan #0" = 50
	"Sardanir #0" = 50
	"Arfiy #0" = 50
	"Armynn #0" = 50
	"Artoray #0" = 50
	"Boreys #0" = 50
	"Bosan #0" = 50
	"Calranval #0" = 50
	"Calranvyr #0" = 50
	"Calenval #0" = 50
	"Calrodiy #4" = 50
	"Camir #0" = 50
	"Camirey #0" = 50
	"Camnik #0" = 50
	"Carodin #0" = 50
	"Celadil #0" = 50
	"Celval #0" = 50
	"Celavyr #0" = 50
	"Drandir #0" = 50
	"Darastay #0" = 50
	"Devynn #0" = 50
	"Dorandir #0" = 50
	"Yeboran #0" = 50
	"Elynn #0" = 50
	"Elecas #0" = 50
	"Elval #0" = 50
	"Elvyr #0" = 50
	"Elran #0" = 50
	"Hranvyr #0" = 50
	"Erandil #0" = 50
	"Hrolas #0" = 50
	"Hrenvyr #0" = 50
	"Elrandil #0" = 50
	"Hrolatas #0" = 50
	"Yevinval #0" = 50
	"Filimir #0" = 50
	"Filenval #0" = 50
	"Fineas #0" = 50
	"Fulvyr #0" = 50
	"Galindan #0" = 50
	"Galiney #0" = 50
	"Galinval #0" = 50
	"Geleney #0" = 50
	"Gelinik #0" = 50
	"Guvnik #0" = 50
	"Yarven #0" = 50
	"Ibenan #0" = 50
	"Irvan #0" = 50
	"Irvandil #0" = 50
	"Irval #0" = 50
	"Yarhel #0" = 50
	"Yarhelval #0" = 50
	"Kyran #0" = 50
	"Liroy #0" = 50
	"Lodisard #0" = 50
	"Lovanc #0" = 50
	"Mayus #0" = 50
	"Momir #0" = 50
	"Munaran #0" = 50
	"Niesthil #0" = 50
	"Orynn #0" = 50
	"Pelomir #0" = 50
	"Privinc #0" = 50
	"Raymun #0" = 50
	"Rey #0" = 50
	"Serondan #0" = 50
	"Serondin #0" = 50
	"Seronval #0" = 50
	"Stanyran #0" = 50
	"Svestynn #0" = 50
	"Telar #0" = 50
	"Telaran #0" = 50
	"Telaryos #0" = 50
	"Talanis #0" = 50
	"Talvynn #0" = 50
	"Telnik #0" = 50
	"Vyrhendir #0" = 50
	"Fasendir #0" = 50
	"Talvyr #0" = 50
	"Trhevyr #0" = 50
	"Trandir #0" = 50
	"Ultaran #0" = 50
	"Yurion #0" = 50
	"Vaceran #0" = 50
	"Varaman #0" = 50
	"Valmynn #0" = 50
	"Varamel #0" = 50
	"Varamelan #0" = 50
	"Varanik #0" = 50
	"Varydal #0" = 50
	"Vindaley #0" = 50
	"Vyrsard #0" = 50
	"Yanobnik #0" = 50
	"Yrisarval #0" = 50
	"Ysaar #0" = 50
	
	"Alara  #0" = -20
	"Alarenka  #0" = -20
	"Alarianvel  #0" = -20
	"Alesadra  #0" = -20
	"Amaredha  #0" = -20
	"Ariadra  #0" = -20
	"Ariadre  #0" = -20
	"Calasadra  #0" = -20
	"Camnava  #0" = -20
	"Celadeva  #0" = -20
	"Devesadra  #0" = -20
	"Eborada  #0" = -20
	"Elara  #0" = -20
	"Elethen  #0" = -20
	"Erezda  #0" = -20
	"Erlanvel  #0" = -20
	"Filinava  #0" = -20
	"Galinvel  #0" = -20
	"Imarezda  #0" = -20
	"Iseheva  #0" = -20
	"Iserana  #0" = -20
	"Iseranvel  #0" = -20
	"Istralana  #0" = -20
	"Ivranvel  #0" = -20
	"Ivranviel  #0" = -20
	"Jexava  #0" = -20
	"Ladrinvel  #0" = -20
	"Lanahava  #0" = -20
	"Lanalora  #0" = -20
	"Leliezda  #0" = -20
	"Leslinvel  #0" = -20
	"Lianden  #0" = -20
	"Merenka  #0" = -20
	"Mihtrenka  #0" = -20
	"Narezda  #0" = -20
	"Nathasadra  #0" = -20
	"Seluzia  #0" = -20
	"Serova  #0" = -20
	"Sharasadra  #0" = -20
	"Sharenka  #0" = -20
	"Sharinvel  #0" = -20
	"Sherenka  #0" = -20
	"Tanelana  #0" = -20
	"Thalanvel  #0" = -20
	"Valanna  #0" = -20
	"Varilvana  #0" = -20
	"Varinna  #0" = -20
	"Varivana  #0" = -20
	"Vehara  #0" = -20
	"Veharazda  #0" = -20
	"Vesiava  #0" = -20
	"Zalenka  #0" = -20
	
}

leader_names = {
	"syn Levo"
	Andyras Arkansyn
	Batyron Becvelar Belyr Bostrenbost Buycev Buyldirsarnik
	Dillanypolere Dohyndav Drantnik
	Epadyr
	Hotnynn Hradapan
	Kanynnyr Kavac Kolar Kralasyr Krilavlai Krilolyv
	Latekyr Lisko Lovoc Lynohur 
	Mecnik Molynar
	Podmanyr Polerbost Polerstadons Pradavytrie Prancybost Prelbuycev Prelvytrie
	Rienar
	Slonyr Spomnik Spyarnik Stretoclesnik Stretocvytrie
	Trenod Trenyr
	Velyk Veselyn Vydrunpolere Vyrekyr
	Ynnvighor Ynnyr Yudatnyvytrie Yusonyr
	Zakavlai
}

ship_names = { 
	Bodrog Bebrava Belá Blava Bodva Dunajec "Cierna voda" Hornád Ipel
	Latorica Morava Ondava Poprad Rimava Slaná Žitava Torysa
}


army_names = {
	"Arverynnic Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Arverynnic Fleet"
}