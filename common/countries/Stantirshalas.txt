#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 213  132  33 }



historical_idea_groups = {
	economic_ideas #farm money
	administrative_ideas #focused on its own territory, also mercenaries
	religious_ideas #harvest festivals
	diplomatic_ideas #patronizing poets
	aristocracy_ideas #strong nobility like most places in the ynn
	spy_ideas #needs to scheme rather than fight head on
	defensive_ideas #same reasoning as admin
	trade_ideas #money always helps
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Dolindhan Names
	"Adrjon #0" = 50
	"Alarn #0" = 50
	"Aldarad #0" = 50
	"Aldirn #0" = 50
	"Alvarad #0" = 50
	"Andrelad #0" = 50
	"Arganvit #0" = 50
	"Ardead #0" = 50
	"Vardha #0" = 50
	"Aganirn #0" = 50
	"Arminjad #0" = 50
	"Artorean #0" = 50
	"Boric #0" = 50
	"Bostjan #0" = 50
	"Bradasred #0" = 50
	"Calsanqir #0" = 50
	"Calasandor #0" = 50
	"Kalarad #0" = 50
	"Calroqir #0" = 50
	"Camovac #0" = 50
	"Komerjon #0" = 50
	"Camndha #0" = 50
	"Kravendil #0" = 50
	"Celadit #0" = 50
	"Celdha #0" = 50
	"Celatan #0" = 50
	"Darantan #0" = 50
	"Darastarad #0" = 50
	"Demirad #0" = 50
	"Dorendan #0" = 50
	"Jeborad #0" = 50
	"Elantan #0" = 50
	"Elead #0" = 50
	"Eldha #0" = 50
	"Elethad #0" = 50
	"Elrad #0" = 50
	"Elranjan #0" = 50
	"Erandit #0" = 50
	"Erecas #0" = 50
	"Erenqir #0" = 50
	"Erlanvit #0" = 50
	"Jevnit #0" = 50
	"Filinej #0" = 50
	"Finoread #0" = 50
	"Galanjon #0" = 50
	"Gamelirn #0" = 50
	"Galadorn #0" = 50
	"Gelendirn #0" = 50
	"Galmovac #0" = 50
	"Jansred #0" = 50
	"Ibenad #0" = 50
	"Ivranvit #0" = 50
	"Ivranqir #0" = 50
	"Ivratan #0" = 50
	"Jearn #0" = 50
	"Jearcad #0" = 50
	"Joanseln #0" = 50
	"Kirjon #0" = 50
	"Lovac #0" = 50
	"Momirn #0" = 50
	"Munaqir #0" = 50
	"Mondirn #0" = 50
	"Morcu #0" = 50
	"Niesthor #0" = 50
	"Oberjon #0" = 50
	"Pavloqir #0" = 50
	"Pelovit #0" = 50
	"Pjodorn #0" = 50
	"Privyldha #0" = 50
	"Rea #0" = 50
	"Suledjarn #0" = 50
	"Serjon #0" = 50
	"Serondor #0" = 50
	"Stantirn #0" = 50
	"Teavirn #0" = 50
	"Talerjon #0" = 50
	"Talarad #0" = 50
	"Telenjon #0" = 50
	"Dhalatan #0" = 50
	"Telrjon #0" = 50
	"Vitraldhur #0" = 50
	"Farendit #0" = 50
	"Rolatan #0" = 50
	"Strestynn #0" = 50
	"Tarkirn #0" = 50
	"Trantan #0" = 50
	"Tranvit #0" = 50
	"Ultarad #0" = 50
	"Jurion #0" = 50
	"Vacerad #0" = 50
	"Varamad #0" = 50
	"Faraji #1" = 50
	"Varmeljon #0" = 50
	"Varamelad #0" = 50
	"Varamit #0" = 50
	"Vatrevid #0" = 50
	"Vylaron #0" = 50
	"Vindirn #0" = 50
	"Vitsred #0" = 50
	"Ysarad #0" = 50
	
	"Alara #0" = -20
	"Alarianvel #0" = -20
	"Alarisa #0" = -20
	"Alesada #0" = -20
	"Amaredha #0" = -20
	"Ariadra #0" = -20
	"Ariadre #0" = -20
	"Calasada #0" = -20
	"Camnava #0" = -20
	"Celadeva #0" = -20
	"Devesada #0" = -20
	"Ebomira #0" = -20
	"Elara #0" = -20
	"Elethen #0" = -20
	"Erezda #0" = -20
	"Erlanvel #0" = -20
	"Filinava #0" = -20
	"Galinvel #0" = -20
	"Imarezda #0" = -20
	"Isehmira #0" = -20
	"Isemira #0" = -20
	"Iseranvel #0" = -20
	"Istramira #0" = -20
	"Ivransada #0" = -20
	"Ivranvel #0" = -20
	"Jexava #0" = -20
	"Ladrinvel #0" = -20
	"Lanahava #0" = -20
	"Lanalora #0" = -20
	"Leliezda #0" = -20
	"Leslinvel #0" = -20
	"Lianden #0" = -20
	"Merisa #0" = -20
	"Mihtrisa #0" = -20
	"Narezda #0" = -20
	"Nathasada #0" = -20
	"Seluzia #0" = -20
	"Serova #0" = -20
	"Shaermira #0" = -20
	"Sharasada #0" = -20
	"Sharinvel #0" = -20
	"Sherisa #0" = -20
	"Tanelana #0" = -20
	"Thalanvel #0" = -20
	"Valanna #0" = -20
	"Varilvana #0" = -20
	"Varinna #0" = -20
	"Varivana #0" = -20
	"Vehamira #0" = -20
	"Vehara #0" = -20
	"Vesiava #0" = -20
	"Zalisa #0" = -20
	
}

leader_names = {
	Babonic Balsic Branivojevic 
	Dragisic
	Gradascevic 
	Hrvatinic
	Isakovic
	Kosaca Kotromanic
	Nelipic
	Pavlovic
	Radinovic 
	Sankovic Santic Sokolovic Subic
	Tvrtkovic
	Vukasin Vukcic Vukovic 
}

ship_names = {
	Bosna Bosni
	"Gazi Husrev-beg"
	"Kotromanici" "Kulin ban"
	Miljacka
	Varivana
	Varinna Zalisa Seluzia
}


army_names = {
	"Stantirshalan Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Stantirshalan Fleet"
}