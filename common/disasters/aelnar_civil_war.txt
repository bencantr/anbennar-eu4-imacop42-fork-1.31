aelnar_civil_war = {
	potential = {
		tag = Z43
		OR = {
			AND = {
				num_of_owned_provinces_with = {
					value = 50
					colonial_region = colonial_noruin
				}
			}
			AND = {
				is_year = 1530
				num_of_owned_provinces_with = {
					value = 30
					colonial_region = colonial_noruin
				}
			}
			any_owned_province = { has_province_flag = aelnar_arca_ore }
			is_year = 1550
		}
		NOT = { has_country_flag = aelnar_civil_war_end }
		NOT = { has_reform = enlightened_empire_reform }
	}

	
	
	can_start = {
		has_any_disaster = no
		OR = {
			NOT = {
				adm = 6
			}
			NOT = {
				dip = 6
			}
			NOT = {
				mil = 6
			}
		}
	}
	
	
	can_stop = {
        has_reform = enlightened_empire_reform
	}
		
	
	
	progress = {
		modifier = {
			factor = 0.8
			NOT = { adm = 6 }
		}
		modifier = {
			factor = 0.8
			NOT = { dip = 6 }
		}
		modifier = {
			factor = 0.8
			NOT = { mil = 6 }
		}
		modifier = {
			factor = 1
			NOT = { ruler_culture = star_elf }
		}
	}
	
	
	
	can_end = {
		custom_trigger_tooltip = {
			tooltip = aelnar_eliendel_must_not_lead
			NOT = { has_ruler = Eliendel }
		}
		custom_trigger_tooltip = {
			tooltip = aelnar_put_down_kayd
			OR = {
				NOT = { check_variable = { kayd_value = 0 } }
				AND = {
					has_country_flag = kayd_revolted
					Z58 = { exists = no }
				}
			}
		}
		
		custom_trigger_tooltip = {
			tooltip = aelnar_put_down_aelthanas
			OR = {
				NOT = { check_variable = { aelthanas_value = 0 } }
				AND = {
					has_country_flag = aelthanas_revolted
					Z59 = { exists = no }
				}
			}
		}
		
		custom_trigger_tooltip = {
			tooltip = aelnar_put_down_sicrheior
			OR = {
				NOT = { check_variable = { sicrheior_value = 0 } }
				AND = {
					has_country_flag = sicrheior_revolted
					Z60 = { exists = no }
				}
			}
		}
		
		custom_trigger_tooltip = {
			tooltip = aelnar_put_down_dahvar
			OR = {
				NOT = { check_variable = { dahvar_value = 0 } }
				AND = {
					has_country_flag = dahvar_revolted
					Z61 = { exists = no }
				}
			}
		}
		
		if = {
			limit = { has_country_flag = lithiel_revolted }
			custom_trigger_tooltip = {
				tooltip = aelnar_put_down_lithiel
				Z62 = { exists = no }
			}
		}
	}
	

	modifier = {
		manpower_recovery_speed = -0.3
		reinforce_speed = -0.1
		reinforce_cost_modifier = 0.25
		stability_cost_modifier = 0.5
		global_tax_modifier = -0.4
		trade_efficiency = -0.4
		production_efficiency = -0.4
		land_morale = -0.15
		interest = 3
		global_unrest = 3
		global_colonial_growth = -100
		heir_chance = -2
	}
	
	
	on_start = aelnar.5
	on_end = aelnar.18
	
	on_monthly = {
		events = {
		}
		random_events = { 
			1000 = 0
			130 = aelnar.16 #Peasant revolt
			90 = aelnar.19 #Noble revolt
		
		}
	}
}

